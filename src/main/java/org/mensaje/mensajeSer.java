package org.mensaje;

import java.util.Scanner;

public class mensajeSer {
    public static void crearMensajes(){
        Scanner S = new Scanner(System.in);
        System.out.println("Escribe tu mensaje");
        String mensaje = S.nextLine();

        System.out.println("Ingresa tu nombre");
        String autor = S.nextLine();

        mensajes registry = new mensajes();
        registry.setMensaje(mensaje);
        registry.setAutor_mensaje(autor);
        MensajeDAO.crearMensajeDB(registry);
    }
    public static void listarMensajes(){
        MensajeDAO.leerMensajesDB();

    }
    public static void borrarMensajes(){
        Scanner S = new Scanner(System.in);
        System.out.println(" Indica cual es el ID del mensaje a borrar");
        int id_mensaje = S.nextInt();
        MensajeDAO.borrarMensajeDB(id_mensaje);
    }
    public static void editarMensajes(){

        Scanner S = new Scanner(System.in);

        System.out.println(" Ingrese el nuevo mensaje");
        String mensaje = S.nextLine();

        System.out.println(" Indica cual es el ID del mensaje a editar");
        int id_mensaje = S.nextInt();

        mensajes actualizacion = new mensajes();
        actualizacion.setId_mensaje(id_mensaje);
        actualizacion.setMensaje(mensaje);
        MensajeDAO.actualizarMensajeDB(actualizacion);


    }



}
