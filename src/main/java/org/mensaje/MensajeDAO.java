package org.mensaje;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajeDAO {

    public static void crearMensajeDB(mensajes  mensaje ){
        conexion db_Connect = new conexion();

        try (Connection conexion = db_Connect.get_connection()){
            PreparedStatement Ps = null;
            try{
                String query = "INSERT INTO mensajes( mensaje,autor_mensaje) VALUES (?,?)";
                Ps = conexion.prepareStatement(query);
                Ps.setString(1, mensaje.getMensaje());
                Ps.setString(2, mensaje.getAutor_mensaje());
                Ps.executeUpdate(); // Envia instrucción a BDD
                System.out.println("Mensaje creado con exito");

            }catch (SQLException ex){
                System.out.println(ex);
            }
        }catch (SQLException e){
            System.out.println(e);
        }
    }

    public static void leerMensajesDB(){
        conexion db_Connect = new conexion();
        PreparedStatement Ps = null;
        ResultSet rs = null;

        try (Connection conexion = db_Connect.get_connection()){
            String query = "SELECT * FROM mensajes";
            Ps = conexion.prepareStatement(query);
            rs = Ps.executeQuery();

            while (rs.next()){
                System.out.println("ID:" + rs.getInt("id_mensaje"));
                System.out.println("Mensaje:" + rs.getString("mensaje"));
                System.out.println("Autor:" + rs.getString("autor_mensaje"));
                System.out.println("Fecha:" + rs.getString("fecha_mensaje"));
                System.out.println("");
            }
        }catch (SQLException e){
            System.out.println(" No se pudieron recuperar los mensajes");
            System.out.println(e);
        }



    }
    public static void borrarMensajeDB(int id_mensaje) {
        conexion db_Connect = new conexion();

        try (Connection conexion = db_Connect.get_connection()) {
            PreparedStatement Ps = null;
            try {
                String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
                Ps = conexion.prepareStatement(query);
                Ps.setInt(1, id_mensaje);
                Ps.executeUpdate();
                System.out.println("El mensaje ha sido eliminado");
            } catch (SQLException e) {
                System.out.println(e);
                System.out.println("NO se pudo eliminar el mensaje");
            }


        } catch (SQLException e) {
            System.out.println(e);

        }
    }

    public static void actualizarMensajeDB(mensajes mensaje){
        conexion db_Connect = new conexion();

        try (Connection conexion = db_Connect.get_connection()) {
            PreparedStatement Ps = null;

            try {
                String query = "UPDATE mensajes SET mensaje  = ?  WHERE id_mensaje = ?";
                Ps = conexion.prepareStatement(query);

                Ps.setString(1, mensaje.getMensaje());
                Ps.setInt(2, mensaje.getId_mensaje());

                Ps.executeUpdate();
                System.out.println("Mensaje actualizado correctamente");

            }catch (SQLException ex){
                System.out.println(ex + "no se pudo actualizar el mensaje");

            }

        }catch (SQLException e) {
            System.out.println(e);
        }


    }

}
